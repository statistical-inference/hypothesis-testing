# Hypothesis Testing in R

This report covers two parts of hypothesis testing using confidence intervals in R.

## Part I: Confidence Interval for Mean and Variance of the Normal Distribution

1. **Importing Data**: The octopus dataset is loaded, focusing on female octopodes' weight.

2. **Selection of Female Octopodes**: Female octopus data is selected.

3. **Sample Estimators**: Sample mean (x_bar) and variance (s2) are calculated.

4. **Descriptive Histogram**: A histogram of female octopodes' weight is generated.

5. **Confidence Interval for Mean**: The confidence interval for the population mean is calculated using a formula and the `t.test()` function.

6. **Confidence Interval for Variance**: The confidence interval for the population variance is computed.

## Part II: Confidence Interval for Proportion and Effect of Confidence

1. **Generation of Binomial Distributions**: 100 samples of size 100 from binomial distributions are generated.

2. **Confidence Intervals**: Confidence intervals for the proportion (p) are calculated at different levels (90%, 95%, 99%) for the binomial distribution.

3. **Percentage of Intervals Containing P**: The code checks how many intervals contain the true value of p.

4. **90% Confidence Interval Plot**: A plot visualizes the confidence intervals, marking intervals that don't contain the true p parameter value.

5. **Number of Intervals Not Including P**: The code demonstrates the limitations of normal distribution estimation for the binomial proportion in specific scenarios.

This report showcases hypothesis testing through confidence intervals for both normal and binomial distributions, and it explores the impact of confidence levels on the results.
